import os
import time

import cv2 as cv
import face_recognition
import numpy as np
from keras.models import model_from_json
import tensorflow as tf

from train import shape


def get_face(img):
    t = face_recognition.face_locations(img, model='cnn')
    if t is not None:
        (top, right, bottom, left) = t[0]
        face = img[top:bottom, left:right]
        return face
    else:
        return None


def get_test_data(path_to_test_data):
    classes = os.listdir(path_to_test_data)
    x_test = []
    y_test = []
    names = []
    for c in classes:
        imgs = os.listdir(os.path.join(path_to_test_data, c))
        for img_name in imgs:
            img = cv.imread(os.path.join(path_to_test_data, c, img_name))
            face = get_face(img)
            if face is not None:
                face = cv.resize(face, shape)
                x_test.append(face)
                y_test.append(int(c))
                names.append(os.path.join(c, img_name))
    return np.asarray(x_test), y_test, names


def classify_images(path_to_test_data):
    classes = os.listdir(path_to_test_data)
    all_time = 0
    count_imgs = 0
    model = load_faceglass_model()
    for c in classes:
        imgs = os.listdir(os.path.join(path_to_test_data, c))
        for img_name in imgs:
            img = cv.imread(os.path.join(path_to_test_data, c, img_name))
            face = get_face(img)
            if face is not None:
                face = np.asarray([cv.resize(face, shape)])
                before = time.time()
                v = model.predict(face)
                after = time.time()
                all_time += after - before
                count_imgs += 1
                if v[0][0] < v[0][1]:
                    print(os.path.join(c, img_name))
    print("Mean time for single images = " + str(all_time / count_imgs))


def load_faceglass_model():
    # Assume that you have 12GB of GPU memory and want to allocate ~4GB:
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
    f = open("model_json.json", "r")
    json_string = f.readline()
    model = model_from_json(json_string)
    model.load_weights("inception_w.h5")
    return model


if __name__ == "__main__":
    classify_images("data/test")
