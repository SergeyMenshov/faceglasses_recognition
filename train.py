from keras.applications.inception_v3 import InceptionV3
from keras.layers import Dense, GlobalAveragePooling2D
from keras.models import Model
from keras.optimizers import SGD
import numpy as np
import cv2 as cv
import keras
import os

shape = (200, 200)


def load_data():
    return np.load("x_train.npy"), np.load("y_train.npy")


# create the base pre-trained model
def train():
    base_model = InceptionV3(weights='imagenet', include_top=False)

    # add a global spatial average pooling layer
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    # let's add a fully-connected layer
    x = Dense(1024, activation='relu')(x)
    # and a logistic layer -- let's say we have 2 classes
    predictions = Dense(2, activation='softmax')(x)

    # this is the model we will train
    model = Model(inputs=base_model.input, outputs=predictions)

    # first: train only the top layers (which were randomly initialized)
    # i.e. freeze all convolutional InceptionV3 layers
    for layer in base_model.layers:
        layer.trainable = False

    # compile the model (should be done *after* setting layers to non-trainable)
    model.compile(optimizer='adam', loss='categorical_crossentropy')

    x_train, y_train = load_data()
    # train the model on the new data for a few epochs
    model.fit(x_train, y_train, batch_size=128, epochs=5, validation_split=0.2, shuffle=True)
    model.save("inception_1_v19.h5")

    # at this point, the top layers are well trained and we can start fine-tuning
    # convolutional layers from inception V3. We will freeze the bottom N layers
    # and train the remaining top layers.

    # we chose to train the top 2 inception blocks, i.e. we will freeze
    # the first 249 layers and unfreeze the rest:
    for layer in model.layers[:249]:
        layer.trainable = False
    for layer in model.layers[249:]:
        layer.trainable = True

    # we need to recompile the model for these modifications to take effect
    # we use SGD with a low learning rate

    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='categorical_crossentropy')

    # we train our model again (this time fine-tuning the top 2 inception blocks
    # alongside the top Dense layers
    model.fit(x_train, y_train, batch_size=128, epochs=5, validation_split=0.2, shuffle=True)

    f = open("model_json.json", "w")
    json_model = model.to_json()
    f.write(json_model)
    f.close()
    model.save_weights("inception_w.h5")


def get_train_data(path_to_train_data, path_to_meta):
    meta = open(path_to_meta, "r")
    x_train = []
    y_train = []
    for v in meta.readlines():
        img_name, c = v.split(" ")
        img = cv.imread(os.path.join(path_to_train_data, img_name))
        img = cv.resize(img, shape)
        x_train.append(img)
        y_train.append(int(c))
    y_train = keras.utils.to_categorical(y_train, num_classes=2)

    return np.asarray(x_train), y_train


def run(path_to_dataset, path_to_meta):
    x_train, y_train = get_train_data(path_to_dataset, path_to_meta)
    np.save("x_train", x_train)
    np.save("y_train", y_train)
    train()

if __name__ == "__main__":
   run("/home/sergej2/MeGlass_120x120", "data/meta.txt")
